#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 10:11:41 2020

@author: dev

    Discord est un logiciel VoIP datant de 2015 et conçu dans un premier temps 
à destination des communautés de joueurs. Sous la forme de serveurs, plusieurs
(dizaines ou bien milliers) d'utilisateurs peuvent se rassembler virtuellement 
et échanger à l'écrit ou en vocal. 
Ces communautés se sont diversifiées ces dernières années, et tournent 
aujourd'hui autour de sujets variés et non plus exclusifs à la culture "web".
Pour un serveur regroupant des milliers de membres, ou même une centaine, il 
est intéressant et utile d'avoir à disposition un outil d'analyse capable
d'identifier les tendances et périodes d'activité, les membres les plus 
impliqués, ou même les salons les plus actifs afin de gérer au mieux le 
développement du serveur et de sa communauté.

    L'objectif est donc de collecter certaines données provenant de serveurs 
pour permettre une visualisation globale de leur activité et de l'engagement 
de leurs membres au cours du temps.
En d'autres termes : proposer un outil d'analyse de communauté basé sur des
statistiques réelles et accessibles.

    Cet outil est matérialisé sous la forme d'un bot, utilisé afin d'analyser 
certaines informations : les messages envoyés ainsi que leur nombre, 
l'activité vocale, le nombre de membres ainsi que les statuts de ces membres,
pour n'en citer qu'une part.

    L'interface du bot, sous forme d'un site web, propose des statistiques et
représentations graphiques tirées de ces données pour en permettre une 
meilleure compréhension. 

Décomposition du projet : 
    - Module servant au fonctionnement du bot et à la récupération des données
    depuis discord (utilisation de asyncio pour ses coroutines et tâches,
    ainsi que de l'API mise à disposition par Discord pour les développeurs)
    - Module de modélisation de la base de données (PostgreSQL + l'ORM Peewee)
    - Module de traitement des données (calculs, analyses, graphiques)
    - Module web servant d'interface au bot (Flask)
"""

import discord
from discord.ext import commands
import asyncio
import giphy_client

import datetime as date
import time
import pandas as pd
import numpy as np

from auth import token_discord, token_giphy
import modelisation_abot as mode
import analyse_abot as an


### TRAITEMENT
giphy_token = token_giphy
api_instance = giphy_client.DefaultApi()


def search_gifs(gif):
    """
    Recherche de gifs via l'API de giphy
    """
    
    response = api_instance.gifs_search_get(giphy_token, 
        gif, limit=3, rating='g')
    lst = list(response.data)
    gif = np.random.choice(lst)

    return gif.url


def server_count():
    """ 
    Retourne la liste des serveurs sur lesquels le bot est présent ainsi 
    que le nombre de serveurs
    """
    
    guilds = []
    for guild in bot.guilds:
        guilds.append(guild.name)
    nb_guilds = len(guilds)
    
    return guilds, nb_guilds


def get_members(ctx):
    """
    Retourne la liste des membres d'un serveur et le nombre de membres
    """
    
    members = []
    for member in ctx.guild.members:
        members.append(member.name)
    nb_members = len(members)
    
    return members, nb_members


# fonction gardée sous le coude pour ressortir les ids user
# def get_ids(members):
#     user_ids = {}
#     for member in members:
#         user_id = member.id
#         user_ids[member] += user_id
#     nb_ids = len(user_ids)
    
#     return nb_ids, user_ids


def add_server(message):
    """
    Ajoute un nouveau server à la db PostgreSQL via Peewee
    
    TODO : 
        - gérer l'update
    """
    
    mode.Server.create(server_id=message.guild.id, 
                       server_name=message.guild.name, 
                       member_count=message.guild.member_count)
    
    
def add_user(message):
    """
    Ajoute un nouvel utilisateur à la db PostgreSQl via Peewee
    
    TODO : 
        - gérer l'update d'un nickname
    """
    
    mode.User.create(user_id=message.author.id, nickname=message.author)
    
    
def add_msg(message):
    """
    Ajoute un msg à la db si l'auteur du msg n'est pas un bot
    Pourquoi il y a un if ? 
    Si le user est déjà en db, ajoute le msg
    Si le user n'est pas en db, ajoute le user et le msg en db
    """
    
    if message.author.bot == False:
        if mode.User.select().where(mode.User.user_id == message.author.id):
           mode.Msg.create(msg_id=message.id, user_id=message.author.id, 
                                channel_id=message.channel.id, 
                                server_id=message.guild.id,
                                date_msg=date.datetime.today())
        else : 
           mode.User.create(user_id = message.author.id, 
                            nickname = message.author)
           mode.Msg.create(msg_id=message.id, user_id=message.author.id, 
                                channel_id=message.channel.id, 
                                server_id=message.guild.id, 
                                date_msg=date.datetime.today())

           
### BOT PART
bot = commands.Bot(command_prefix='.', description='noice')
# ------------------ BOT EVENT ----------------
@bot.event
async def on_ready():
    """
    Prints de démarrage du bot
    """
    
    print("Connexion ...")
    print('- Connexion établie en tant que {0.user} -'.format(bot))
    servers, nb_servers = server_count()
    print("- Project version : bot / prefix is '.' -")
    print('- Connecté avec succès aux serveurs : ', servers, ' -')
    await bot.change_presence(activity=discord.Game(name="try .aide"))
    print('- ',date.datetime.now(), ' : En ligne et opérationnel !')
    

    
@bot.event
async def on_command_error(ctx, error):
    """
    Gestion des erreurs de cmds:
        renvoie un gif si la cmd commence uniquement par un '.'
    """
    
    if isinstance(error, commands.CommandNotFound):
        if ctx.message.content.startswith(".."):
            return
        else:
            async with ctx.channel.typing():
                gif = search_gifs(ctx.message.content)
                await asyncio.sleep(1)
                await ctx.send(gif)


@bot.event
async def on_voice_state_update(member, before, after):
    """
    Fonction appelée dès lors que le voice statut d'un utilisateur change 
    (connexion en vocal, déconnexion, mute, deafen).
    A terme, stockera les logs dans une dataframe temporaire pour upload tous
    les X time en db.
    
    OUI JE SAIS LES 80 CHARACS MAIS C'EST DES PRINTS DE TEST PROMIS
    """
    
    # df = pd.DataFrame(columns=['user', 'channel', 'joined', 'left'])
    if not before.channel:
        joined = date.datetime.now()
        print(f'{date.datetime.now()} - {member.name} has joined {after.channel.name}')
        # print(f'dataframe ready, values : {member.id}, {after.channel.id}, {joined}')
        # new_row = {'user': member.id, 'channel':after.channel.id, 'joined':joined}
        # df = df.append(new_row, ignore_index=True)
        # print(df)
        # mode.VoiceStat.create(vc_id=before.channel.id, user_id=member.id, 
        #                    datetime_joined=date.datetime.now())
        
# comment je fais un update de row précis sans le garder en mémoire shit
# je peux pas aller chercher l'id au pif
    if before.channel and not after.channel:
        left = date.datetime.now()
        print(f'{date.datetime.now()} - {member.name} has left {before.channel.name}')
        # mask = (df['user'] == member.id) 
        #         & (df['channel'] == before.channel.id) 
        #         & (df['left'].isnull())
        # df.loc[mask, 'left'] = left
        # print(df)
        # q = (mode.VoiceStat.update(datetime_left = date.datetime.now())
        #                      .where(user_id == member.id))
        
    if before.channel and after.channel:
        left = date.datetime.now()
        print(f"""{date.datetime.now()} - {member.name} has left {before.channel.name} and joined {after.channel.name}""")
        # mask = (df['user'] == member.id) 
        #         & (df['channel'] == before.channel.id) 
        #         & (df['left'].isnull())
        # df.loc[mask, 'left'] = left
        # print(df)
        # joined = date.datetime.now()
        # print('état arrivée df : {df}')
        # print(f'{member.name} has joined {after.channel.name}')
        # print(f'dataframe ready, values : {member.id}, {after.channel.id}, {joined}')
        # new_row = {'user': member.id, 'channel':after.channel.id, 
        #            'joined':joined}
        # df = df.append(new_row, ignore_index=True)
        # print(df)
        
@bot.event
async def on_message(message):
    """
    Fonction appelé dès qu'un message est reçu sur un serveur
    Vérifie que le msg n'a pas été envoyé par ce bot ou un autre, puis add le 
    msg et/ou le serveur et/ou l'utilisateur en db
    """

    if message.author == bot.user:
        return   
    
    if message.guild is None and not message.author.bot:
        return
    
    if not mode.Server.select().where(mode.Server.server_id 
                                      == message.guild.id):
        add_server(message)
    
    # si le msg ne commence pas par . (= ce n'est pas une cmd)
    if not message.content.startswith('.'):
        if mode.User.select().where(mode.User.user_id == message.author.id):
            add_msg(message)
        else :
            add_user(message)
            add_msg(message) 
   
    # ANCIENNE VERSION DU CODE CI-DESSOUS
        
    # # si l'auteur du msg est déjà enregistré en bdd ajoute seulement son msg
    # if mode.User.select().where(mode.User.user_id == message.author.id):
    #     if not message.startswith('.'):
    #         add_msg(message)
    # # si pas enregistré, enregistre l'auteur en bdd et le msg
    # else : 
    #     if not message.startswith('.'):
    #         add_user(message)
    #         add_msg(message)   
    #     else :
    #         add_user(message)
            
    # print('-'*5 + ' msg add // auteur : '
    #       + message.author.name + ' // '
    #       + str(date.datetime.today()))
        
    await bot.process_commands(message)
    
    
@bot.listen()
async def on_message(message):
    """
    Listener des events on_message, utilisé uniquement pour le fun désolée
    Si un msg est envoyé en dm au bot, il lui répond avec l'une des réponses
    pré déterminées
    """
    
    reponses = ['careful, someone is listening to us', 
                'can\'t talk right now sorry', 
                'hope you\'re having a great day', 
                'why you sliding in my dms ??', 
                'what do you want AGAIN', 'ok fine', 
                'am busy, come back later', 
                'just doing my job']
    if message.author == bot.user:
        return
    if message.guild is None and not message.author.bot:
       print('message reçu de ', message.author.name, ': ', message.content)
       async with message.channel.typing():
           rep = np.random.choice(reponses, 1)
           await asyncio.sleep(1)
           await message.channel.send(*rep)
           print('réponse du bot : ', *rep)
       # print('-'*5+' le dernier message reçu était un dm, pas add à la db')
           
       # if message.author.id == 461315355206352896:
       #   # msg channel = général du serv digifab
       #   msg_channel = 689085054718181435
       #   channel = bot.get_channel(msg_channel)
       #   await channel.send(message.content)
        

# ------------------ BOT COMMAND ----------------
@bot.command()
async def hello(ctx):
    """
    Cmd de ping du bot, répond simplement à l'utilisateur si utilisée sur un
    serveur
    """
    
    async with ctx.channel.typing():
        await asyncio.sleep(1)
        await ctx.send('busy day <@'+str(ctx.message.author.id)
                       +'>, talk to you later')


@bot.command()
async def sup(ctx):
    """
    Cmd pour retourner sur cb de serveurs se trouve le bot
    """
    
    servers, nb_servers = server_count()
    async with ctx.channel.typing():
        await asyncio.sleep(1)
        await ctx.send('watching over '+ str(nb_servers) 
                                   +' servers.')

@bot.command()
async def members(ctx):
    """
    Cmd pour obtenir le nombre de membres ainsi que la liste
    """
    
    async with ctx.channel.typing():
        await asyncio.sleep(1)
        members, nb_members = get_members(ctx)
        await ctx.send(f'there are {nb_members} members on this server, '+
                       f'here is the list : {members}')
    

@bot.command()
async def graph(ctx, duree, nb):
    """ 
    Cmd pour afficher le barplot de répartition des msgs en fonction des
    membres
    Prend en args une durée exprimée en JOURS (par ex : 15 -> 15 jours avant
    la date d'aujourd'hui)
    Ainsi qu'un nombre d'utilisateur à afficher sur le graph 
    (par ex : 5 -> 5 premiers utilisateurs ayant envoyé le plus de msgs)
    """
    
    async with ctx.channel.typing():
        server = ctx.guild.id
        timer_msg = time.perf_counter()
        df = an.stats_msgs_server(server, int(duree))
        # fin timer
        timer_msg = time.perf_counter() - timer_msg
        perf = open('performances_recup_donnees.txt', "a")
        perf.write('temps récupération données pour '+ duree +' jours : '+ str(timer_msg)+ '\n')
        perf.close()
        print(f"temps d'exec : {timer_msg}")
        an.barplot_repartition_msg(df, int(nb))
        await ctx.send(file=discord.File('static/images/image.png'))
        
        
@bot.command()
async def activity(ctx, critere, duree):
    """
    Cmd pour afficher les courbes d'activité
    Prend en arg un critère (daily -> affiche la courbe en fonction des heures
                            weekly -> affiche la courbe en fonction des jours)
    Ainsi qu'une durée exprimée en JOURS (par ex : 15 -> 15 jours avant la 
    date d'aujourd'hui)
    """
    
    async with ctx.channel.typing():
        criteres = ['daily', 'weekly']
        if critere not in criteres:
            return
        server = ctx.guild.id
        df = an.recup_msgs(server, int(duree))
        an.line_activite(df, critere)
        await ctx.send(file=discord.File('static/images/activity-line.png'))
        
@bot.command()
async def projet(ctx, proj):
    """
    Cmd retournant les liens gitlab des deux projets collectifs de la promo
    Prend en arg un projet (digifood/arbre)
    """
    
    async with ctx.channel.typing():
        if proj == 'digifood' :
            lien = 'https://gitlab.com/digifab/promo_2020/projets/digifoodfact'
        if proj == 'arbre':
            lien = 'https://gitlab.com/digifab/promo_2020/'
            lien += 'projets/arbre-de-competences-v3'
        await asyncio.sleep(1)
        await ctx.send("here's the link : "+ str(lien))


@bot.command()
async def clear(ctx, nb=3):
    """
    Cmd utilisée pour purger les derniers msgs envoyés 
    Utilisable uniquement par moi pour l'instant (plus tard par un admin)
    Prend en arg un nombre de messages à effacer (ARME DE DESTRUCTION MASSIVE)
    Par défaut le nb de msgs est fixé à 3 (car le msg invoquant la cmd 
    compte (1), le msg envoyé pour le bot aussi (2) et le msg visé (3))
    """
    
    if ctx.message.author.id == 461315355206352896:
        msgs = []
        async for msg in ctx.channel.history(limit=int(nb)):
            msgs.append(msg)
        await ctx.channel.delete_messages(msgs)
        await ctx.send(str(nb) + ' message(s) effacé(s).')


@bot.command()
async def lurkers(ctx, duree):
    """
    Cmd qui renvoie le ratio des utilisateurs actifs:passifs du serveur
    Prend en arg une durée en JOURS sur laquelle faire le calcul
    .lurkers 3 -> renvoie le ratio pour les 3 derniers jours
    """
    
    async with ctx.channel.typing():
        membres, nb_membres = get_members(ctx)
        lurkers = an.ratio_lurkers(ctx.guild.id, int(duree), nb_membres)
        await asyncio.sleep(1)
        await ctx.send(str(lurkers)+'% des membres de '
                       + str(ctx.guild) + ' ont été actifs au cours des ' 
                       +str(duree)+ ' derniers jours.')
        
    
@bot.command()
async def aide(ctx):
    """
    Cmd à utiliser sur le serveur pour que le bot envoie son aide.txt en dm
    à l'utilisateur
    """
    
    async with ctx.channel.typing():
        doc = open('aide.txt', 'r')
        aide = doc.read()
        await ctx.author.send(aide)
        await asyncio.sleep(1)
        doc.close()
        await ctx.send('c\'est envoyé, vérifie tes messages privés <@'
                       +str(ctx.message.author.id)+'> !')

@bot.command()
async def user_info(ctx, user: discord.Member):
    await ctx.send(f'The username of the user is {user.name}')
    await ctx.send(f'The ID of the user is {user.id}')
    await ctx.send(f'The status of the user is {user.status}')
    await ctx.send(f'The role of the user is {user.top_role}')
    await ctx.send(f'The user joined at {user.joined_at}')
    await ctx.send(f'The account was created at {user.created_at}')
    
bot.run(token_discord)