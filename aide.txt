Hello !

L'objectif premier de ce bot est de récupérer et stocker les données mises à disposition par discord pour proposer une analyse statistique de la communauté d'un serveur.

**Commandes supportées :**
`.hello` > juste pour vérifier que le bot répond bien
`.sup` > sur combien de serveurs le bot est-il présent ? 
`.members` > retourne le nombre de membres sur ce serveur ainsi que leur liste
`.graph nombre_de_jours nombre_membres` > génère un histogramme de répartition des messages (peut prendre un peu de temps), pour les x derniers jours, par ex : `.graph 5 5` affiche le top 5 du serveur sur les 5 derniers jours
`.activity critère nombre_de_jours` > génère une courbe d'activité du serveur, les critères disponibles sont `daily` (activité au cours de la journée) ou `weekly` (activité au cours de la semaine), par ex : `.activity daily 30` affiche la courbe d'activité au cours de la journée pour les 30 derniers jours
`.projet nomduprojet` > par exemple : .projet digifood ou .projet arbre de compétence, retourne un .zip des fichiers du projet
`.lurkers nombre_de_jours` > renvoie le ratio actifs:passifs du serveur pour la période spécifiée
`.aide` > envoie ce message en dm

** Les commandes ne sont utilisables que sur le serveur **

Toutes les invocations de commandes non reconnues, `.blabla` par exemple, auront pour réponse un gif.
Bref, on peut aussi s'en servir pour envoyer des gifs plus rapidement (je décline toute responsabilité en cas d'incident).