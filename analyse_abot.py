#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 12:20:07 2020

@author: dev

Module de traitement des données :

        Les calculs, graphiques et traitement de données nécessaires 

"""
import peewee
from playhouse.postgres_ext import PostgresqlExtDatabase
import numpy as np
import pandas as pd
import datetime as date

import seaborn as sns
import matplotlib.pyplot as plt
import pygal

from modelisation_abot import Msg, User


db = PostgresqlExtDatabase(host="localhost",
                            database="db_discord",
                            user="postgres",
                            password="1234",
                            port=5432)


### TRAITEMENT / RECUPÉRATION DONNÉES
def stats_msgs_server(server, duree):
    """
    Va chercher en db les msgs des utilisateurs d'un serveur pour une durée
    donnée et calcule la répartition des msgs
    """
    
    top_server = pd.DataFrame(columns=['user', 'nb messages', 'date observée'])
    ajd = date.datetime.now()
    date_debut = ajd - date.timedelta(duree)
    users = list(User.select(User.user_id, User.nickname)
                        .join(Msg)
                        .where((Msg.server_id == server) 
                               & (date_debut <= Msg.date_msg) 
                               & (Msg.date_msg <= ajd))
                        .distinct())
    for user in users:
        nb_msg = (Msg.select(User.nickname)
                  .join(User)
                  .where((Msg.server_id == server) 
                         &(Msg.user_id == user) 
                         & (date_debut <= Msg.date_msg) 
                         & (Msg.date_msg <= ajd)).count())
        dico = {'user' : user.nickname, 
                'nb messages': nb_msg, 
                'date observée': date.datetime.today()}
        top_server = top_server.append(dico, ignore_index=True)
        top_server = top_server.sort_values(by='nb messages', ascending=False)
        
    return top_server



def tri_msg(server, duree, membres):
    """ Récupère de la db les stats générales du serveur des msgs par 
    utilisateur mais aussi les stats spécifiques aux channels 
    
    TODO :
        - à fusionner avec la fonction stats_msgs_server
    """
    
    top_server = pd.DataFrame(columns =['user', 'nb messages', 'channel', 
                                        'date observée'])
    ajd = date.datetime.now()
    date_debut = ajd - date.timedelta(duree)
    # users = list(User.select(User.user_id, User.nickname).where(User.nickname == membres))
    # # liste des users unique ayant déjà envoyé un msg (donc stockés en bdd)
    users = list(User.select(User.user_id, User.nickname)
                        .join(Msg)
                        .where((Msg.server_id == server) 
                                & (date_debut <= Msg.date_msg) 
                                & (Msg.date_msg <= ajd))
                        .distinct())
    # liste des channels
    channels = list(Msg.select(Msg.channel_id)
              .where((Msg.server_id == server) 
                          & (date_debut <= Msg.date_msg) 
                          & (Msg.date_msg <= ajd))
              .distinct())
                 
    for user in users:
        for ch in channels:
            nb_msg = (Msg.select(User.nickname)
                  .join(User)
                  .where((Msg.server_id == server) 
                          & (Msg.user_id == user) 
                          & (Msg.channel_id == ch.channel_id)
                          & (date_debut <= Msg.date_msg) 
                          & (Msg.date_msg <= ajd)).count())
            dico = {'user' : user.nickname, 
                    'nb messages': nb_msg, 
                    'channel': ch.channel_id,
                    'date observée': date.datetime.today()}
            top_server = top_server.append(dico, ignore_index=True)
            top_server = top_server.sort_values(by='nb messages', 
                                                ascending=False)
    
    top_server_main = pd.DataFrame(columns =['user', 'nb messages', 
                                             'date observée'])
    for user in users:
            nb_msg = (Msg.select(User.nickname)
                      .join(User)
                      .where((Msg.server_id == server) 
                              & (Msg.user_id == user) 
                              & (date_debut <= Msg.date_msg) 
                              & (Msg.date_msg <= ajd)).count())
            dico = {'user' : user.nickname, 
                    'nb messages': nb_msg, 
                    'date observée': date.datetime.today()}
            top_server_main = top_server_main.append(dico, ignore_index=True)
            top_server_main = top_server_main.sort_values(by='nb messages', 
                                                          ascending=False)
    
    return top_server_main, top_server
            

def recup_msgs(server, duree):
    """ 
    Récupère les msgs d'un serveur pour une durée donnée et traite la colonne
    date_msg de façon à la séparer en 2 colonnes (date / heure)
    
    TODO :
        - à fusionner avec stats_msgs_server
    """
    
    df_msgs = pd.DataFrame()
    # msgs = pd.DataFrame(columns =['user', 'nb messages', 'date'])
    ajd = date.datetime.now()
    date_debut = ajd - date.timedelta(duree)
    users = list(User.select(User.user_id, User.nickname)
                        .join(Msg)
                        .where((Msg.server_id == server) 
                                & (date_debut <= Msg.date_msg) 
                                & (Msg.date_msg <= ajd))
                        .distinct())
    for user in users:
        msg = list(Msg.select(User.nickname, 
                                  Msg.date_msg, 
                                  Msg.channel_id)
                  .join(User)
                  .where((Msg.server_id == server) 
                          & (Msg.user_id == user) 
                          & (date_debut <= Msg.date_msg) 
                          & (Msg.date_msg <= ajd)).dicts())
        msgs = pd.DataFrame.from_dict(msg)
        df_msgs = df_msgs.append(msgs)
        
    df_msgs['date'] = pd.to_datetime(df_msgs['date_msg'], 
                                     format='%Y:%M:%D').dt.date
    df_msgs['heure'] = pd.to_datetime(df_msgs['date_msg'], 
                                      format='%Y:%M:%D').dt.hour
    
    return df_msgs


def tri_activite(dataframe, critere):
    """
    A partir d'un dataframe mise en forme par la fonction recups_msgs, cette
    fonction calcule les moyennes d'activité (envoi de msgs) selon deux
    critères :
        daily -> calcule les moyennes heure par heure de la journée
        weekly -> calcule la même chose jour par jour de la semaine
        
    Appelée pour tracer les courbes avec line_activite
    """
    
    if critere == 'weekly':
        msg_activity = pd.DataFrame()
        dataframe['weekday'] = dataframe['date_msg'].dt.dayofweek
        msg_activity['total msg'] = dataframe['weekday'].value_counts()
        for day in dataframe['weekday'].unique():
            msg_count_date = pd.DataFrame()
            msg_count_date = dataframe[dataframe['weekday'] == day]
            nb_dates = len(msg_count_date['date'].unique())
            msg_activity['moy msg'] = (msg_activity['total msg'] / nb_dates)
            msg_activity.sort_index()
            
    if critere == 'daily':
        msg_activity = pd.DataFrame()
        dates = list(dataframe['date'].unique())
        for date_msg in dates:
            count = pd.DataFrame()
            msg_count_date = pd.DataFrame()
            msg_count_date = dataframe[dataframe['date'] == date_msg]
            # pas écraser la df compte_heure
            count[date_msg] = msg_count_date['heure'].value_counts()
            msg_activity = msg_activity.append(count)
        msg_activity.insert (0, 'heure', msg_activity.index)
        msg_activity.fillna(0).sort_index()
        msg_activity = msg_activity.groupby(['heure']).agg('sum')
        # msg_activity = msg_activity.groupby(['heure']).agg('mean')
        msg_activity['moy msg'] = msg_activity.mean(axis=1)
        
    return msg_activity


def actifs(server, duree):
    """
    Fonction liée à la fonction ratio_lurkers, calcule le nombre de membres
    actifs d'un serveur pour une durée donnée
    Càd les membres ayant envoyé au moins un msg pendant la durée indiquée
    """
    
    ajd = date.datetime.now()
    date_debut = ajd - date.timedelta(duree)
    users = list(User.select(User.user_id, User.nickname)
                        .join(Msg)
                        .where((Msg.server_id == server) 
                               & (date_debut <= Msg.date_msg) 
                               & (Msg.date_msg <= ajd))
                        .distinct().dicts())
    nb_actifs = len(users)
    
    return nb_actifs


def ratio_lurkers(server, duree, total_membres):
    """
    Calcul du ratio actifs:passifs d'un serveur
    """
    
    nb_actifs = actifs(server, duree)
    ratio = nb_actifs*100/total_membres
    
    return round(ratio)

def profil_membre(membre, serveur, duree):
    ajd = date.datetime.now()
    date_debut = ajd - date.timedelta(duree)
    
    # données générales pour le donut graph
    msgs_serveur = stats_msgs_server(serveur, duree)
    total_msgs = int(msgs_serveur['nb messages'].sum())
    msgs_membre = int(msgs_serveur.loc[msgs_serveur['user'] == membre,['nb messages']].values)
    msgs_autres = total_msgs - msgs_membre
    data_pie = {'messages membre': msgs_membre, 'messages autres': msgs_autres}
    
    fig, ax = plt.subplots()
    ax.axis('equal')
    cmap = plt.get_cmap("tab20c")
    outer_colors = cmap(np.arange(len(data_pie)))
    mypie, _ = ax.pie(data_pie.values(), radius=1.3, labels=data_pie.keys(), colors=outer_colors)
    plt.setp(mypie, width=0.3, edgecolor='white')
    plt.margins(0,0)
    # show it
    plt.show()
    
    date_debut = date.datetime(2020, 8, 1)
    # données spécifiques aux chs pour le donut graph
    # récupération de l'id du membre
    user = list(User.select(User.user_id, User.nickname)
                .where(User.nickname == membre))
    # liste des chs du serveur
    channels = list(Msg.select(Msg.channel_id)
             .where((Msg.server_id == server) 
                         & (date_debut <= Msg.date_msg) 
                         & (Msg.date_msg <= ajd))
             .distinct())
    
    top_server = pd.DataFrame()
    for ch in channels:
        nb_msg = (Msg.select(User.nickname)
              .join(User)
              .where((Msg.server_id == server) 
                      & (Msg.user_id == user) 
                      & (Msg.channel_id == ch.channel_id)
                      & (date_debut <= Msg.date_msg) 
                      & (Msg.date_msg <= ajd)).count())
        dico = {'user' : user[0].nickname, 
                'nb messages': nb_msg, 
                'channel': ch.channel_id,
                'date observée': date.datetime.today()}
        top_server = top_server.append(dico, ignore_index=True)
        top_server = top_server.sort_values(by='nb messages', 
                                            ascending=False)
        
    inner_colors = cmap(np.arange(len(dico)))
    mypie, _ = ax.pie(data_pie.values(), radius=1.3, labels=data_pie.keys(), 
                      colors=outer_colors)
    plt.setp(mypie, width=0.3, edgecolor='white')
    # Second Ring (Inside)
    mypie2, _ = ax.pie(dico['nb messages'], radius=1.3-0.3, 
                        labels=dico['channel'], labeldistance=0.7, 
                        colors=inner_colors)
    plt.setp(mypie2, width=0.4, edgecolor='white')
    plt.margins(0,0)
    # show it
    plt.show()
### ANALYSES GRAPHIQUES


def barplot_repartition_msg(df, top_cb=30):
    df = df.head(top_cb)
    plt.clf()
    graph = sns.barplot(x="user", y="nb messages", data=df)
    graph.tick_params(axis='x', labelrotation=5*len(df))
    graph.set(xlabel=" ", ylabel = "Nombre de messages")
    plt.title("Nombre de messages envoyés en fonction des membres", 
              size=15)
    plt.setp(graph.get_xticklabels(), rotation=2.5*len(df), ha="right",
             rotation_mode="anchor")
    plt.tight_layout()
    # plt.show()
    plt.savefig('static/images/image.png')
    
    
def line_activite(df, critere):
    """
    Trace la courbe de l'activité du serv en fonction des moyennes de msgs
    envoyés sur une journée/semaine
    """
    
    df = tri_activite(df, critere)
    plt.clf()
    if critere == "weekly":
        labels = ['lundi', 'mardi', 'mercredi', 'jeudi', 
           'vendredi', 'samedi', 'dimanche']
        plt.xticks(np.arange(len(labels)), labels)
    if critere == "daily":
        labels = df.index
        plt.xticks(labels)
         
    y = df['moy msg']
    x = df.index
    ax = sns.lineplot(x=x, y=y, data=df)
    plt.setp(ax.get_xticklabels(), rotation=2.5*len(x), ha="right",
                 rotation_mode="anchor")
    plt.grid(alpha=0.2, linestyle='--')
    plt.title('Activité moyenne sur la période')
    plt.tight_layout()
    plt.savefig('static/images/activity-line.png')


def donut_repartition_msg(msg_general, msg_channel, top_cb=5):
    """
    Trace deux graphiques pie surperposés afin de pouvoir comparer les stats
    d'envoi de message générales au serveur et spécifiques aux channels d'un 
    seul coup
    
    TODO:
        - GÉRER LA CMAP
    """
    
    df = pd.DataFrame()
    top5 = list(msg_general['user'].head(top_cb))
    for x in top5:
        rows = msg_channel.loc[msg_channel['user'] == x]
        df = df.append(rows)
    # on se débarasse des lignes où le nb de msgs est à 0
    df = df[df['nb messages'] != 0]
    

    # First Ring (outside)
    fig, ax = plt.subplots()
    ax.axis('equal')
    cmap = plt.get_cmap("tab20c")
    outer_colors = cmap(np.arange(len(top5)))
    inner_colors = cmap(np.arange(len(df['user'])))
    
    mypie, _ = ax.pie(msg_general['nb messages'].head(top_cb), radius=1.3, 
                      labels=msg_general['user'].head(top_cb), 
                      colors=outer_colors)
    plt.setp(mypie, width=0.3, edgecolor='white')
    # Second Ring (Inside)
    mypie2, _ = ax.pie(df['nb messages'], radius=1.3-0.3, 
                        labels=df['channel'], labeldistance=0.7, 
                        colors=inner_colors)
    plt.setp(mypie2, width=0.4, edgecolor='white')
    plt.margins(0,0)
    # show it
    plt.show()


### GRAPHIQUES POUR WEB
def graph_repartition(df, top_cb=30):
    barChart = pygal.Bar(height=400)
    general = df.head(top_cb)
    for x in general['user']:
        nb_msg = general.loc[general['user'] == x, ['nb messages']]
        barChart.add(x, nb_msg['nb messages'])
    barChart.render_in_browser()
    
def graph_activity(df, critere):
    df = tri_activite(df, critere)
    print(df)
    df = df.sort_index()
    df = df.round()
    line = pygal.Line(height=400, legend_at_bottom=True, truncate_legend=-1)
    line.title = "Activité générale"
    labels = []
    if critere == 'daily':
        for elem in df.index:
            labels += [str(elem) + 'h']
    if critere == 'weekly':
        labels += ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 
                       'samedi', 'dimanche']
    line.x_labels = labels
    line.add("Quantité moyenne de messages", df['moy msg'])
    line.render_in_browser()

import pandas as pd
import time
import psycopg2

### TEST PERF PEEWEE
timer_msg = time.perf_counter()
query = Msg.select().where(Msg.server_id==922337203685477).dicts()
df = pd.DataFrame(list(query))
timer_msg = time.perf_counter() - timer_msg
print(f"temps d'exec : {timer_msg}")

Msg.add_index(Msg.server_id)

### TEST PERF PSYCOPG2
conn = psycopg2.connect(user="postgres",
                                  password="1234",
                                  host="localhost",
                                  port="5432",
                                  database="test_discord")
cur = conn.cursor()
timer_msg = time.perf_counter()
sql = 'SELECT * FROM msg WHERE server_id = 922337203685477'
result = pd.read_sql_query(sql, conn)
timer_msg = time.perf_counter() - timer_msg
print(result)
print(timer_msg)

if __name__ == '__main__':
    chargement_msgs = False
    moyenne_activite = False
    lineplot = False
    barplot = False
    nested_donut = False
    pygal_repartition = False
    pygal_activity = False
    
    critere = 'daily'
    server = 689085054718181394
    duree = 30
    
    if chargement_msgs:
        msgs = recup_msgs(server, duree)
        
    if moyenne_activite:
        msgs = recup_msgs(server, duree)
        stats_msgs = tri_activite(msgs, critere)
        
    if lineplot:
        msgs = recup_msgs(server, duree)
        line_activite(msgs, critere)
        
    if barplot:
        top_cb = 5
        msgs = stats_msgs_server(server, duree)
        barplot_repartition_msg(msgs, top_cb)
        
    if nested_donut:
        membres = 'Léa#9250'
        df_gen, df_ch = tri_msg(server, duree, membres)
        donut_repartition_msg(df_gen, df_ch)
    
    if pygal_repartition:
        msgs = stats_msgs_server(server, duree)
        graph_repartition(msgs, top_cb=30)
        
    if pygal_activity:
        msgs = recup_msgs(server, duree)
        graph_activity(msgs, critere)
       