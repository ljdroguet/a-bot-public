#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 15:30:17 2020

@author: dev

Script indépendant de sauvegarde automatique de la base de données PostgreSQL,
un fichier .sql est exporté toutes les 12 heures dans le dossier db_backups.
"""
import sched
import os 
import datetime as date
import modelisation_abot as mode
from auth import mdp_postgres


def sauvegarde(nom_fichier='a_bot'):
    """
    Crée un fichier de sauvegarde .sql de la base de données du bot, ce script
    doit être éxecuté à part et en continu.
    Les logs des sauvegardes sont enregistrés en bdd.
    """
    
    mtn = date.datetime.now()
    mtn_str = mtn.strftime("_%Y-%m-%d_%H-%M-%S")
    nom_fichier = nom_fichier + mtn_str
    
    def logs_sauvegarde(status):
        """
        Enregistre en bdd les infos de chaque sauvegarde : nom du fichier,
        heure et date, status (succès ou échec). Les logs sont enregistrés
        sur la table "backuplog".
        """
        
        mode.BackupLog.create(file_name=nom_fichier, backup_date=mtn, 
                              status=status)
 
    cmd = f"""PATH="$PATH:/Library/PostgreSQL/12/bin";
            PGPASSWORD={mdp_postgres} pg_dump -U postgres -F p db_discord > db_backups/{nom_fichier}.sql"""
    os.system(cmd)
    # check si le fichier existe bien (donc si le backup a été un succès)
    if os.path.isfile("db_backups/"+nom_fichier+'.sql'):
        print('backup successful')
        status = 'completed'
    else : 
        print('backup failed')
        status = 'failed'
        
    logs_sauvegarde(status)
    

if __name__ == "__main__":
    sauvegarde_on = True

    if sauvegarde_on: 
        debut = date.datetime.now()   
        s = sched.scheduler()
        # count = 0
        while True:
            s.enter(43200, 1, sauvegarde)
            s.run()
            # count += 1
        
   