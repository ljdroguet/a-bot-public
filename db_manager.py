#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 14:19:47 2020

@author: dev

Ce module permet la restauration sur une NOUVELLE base de données d'une
ancienne sauvegarde .sql, structure et données comprises.
"""

import os 
from auth import mdp_postgres, mdp_user

def restauration(bdd, fichier):
    """ 
    Restaure une sauvegarde .sql de la bdd sur une nouvelle bdd créée 
    à partir du nom passé en arg 
    
    bdd : nom de la nouvelle bdd à créer 
    fichier : chemin du fichier à partir duquel restaurer
    """
    
    cmd = f"""
            sudo -S <<< {mdp_user} command sudo -u postgres 
            PGPASSWORD={mdp_postgres} /Library/PostgreSQL/12/bin/createdb {bdd};
            PGPASSWORD={mdp_postgres} /Library/PostgreSQL/12/bin/psql -U 
            postgres {bdd} < {fichier}"""
    os.system(cmd)
   
if __name__ == "__main__":
    recharge_on = False
    fichier = '/Users/test/Documents/A_BOT_PUBLIC_V2/db_backups/a_bot_2020-09-04_01-53-41.sql'
    if recharge_on:
       restauration('test_restauration', fichier)
