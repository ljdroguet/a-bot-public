#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 09:07:50 2020

@author: dev
"""
from flask import (Flask, render_template, request, redirect, session)

import datetime as date

import analyse_abot as an

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def accueil():
    title = "A bot"
    serveur = 689085054718181394
    
    if request.method == 'POST':
        nb_max = request.form['users']
        date_debut = date.datetime.strptime(request.form['date_debut'],
                                            "%Y-%m-%d")
    else : 
        date_debut = date.datetime(2020, 8, 1)
        # besoin d'un int positif
        nb_max = 10
        
    ajd = date.datetime.now()
    duree = abs(date_debut - ajd).days
    
    imgs_right = []
    imgs_left = []
    
    # msgs = an.recup_msgs(serveur, duree)
    # an.graph_repartition(msgs, int(nb_max))
    # imgs_right += ['image.png']
     
    # df = an.stats_msgs_server(serveur, duree)
    # an.barplot_repartition_msg(df, int(nb_max))
    # imgs_right += ['image.png']
    
    return render_template("index.html", title=title, imgs_right=imgs_right, 
                           imgs_left=imgs_left)
if __name__ == '__main__':
    app.run(debug=True)
