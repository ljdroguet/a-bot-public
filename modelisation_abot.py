#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 29 16:20:09 2020

@author: dev

Modélisation de la base de données pour PostgreSQL
Les statistiques des serveurs y sont envoyées et stockées grâce à l'ORM Peewee.

je finirai ça plus tard
"""
# Imports

# Bibliothèque externe
from peewee import Model
from peewee import (AutoField, DateField, TextField, IntegerField,
                    CharField, ForeignKeyField, DateTimeField, TimeField,
                    PrimaryKeyField, BigIntegerField, BigAutoField)
from playhouse.postgres_ext import PostgresqlExtDatabase


# Connecteur postgres
db = PostgresqlExtDatabase(host="localhost",
                            database="db_discord",
                            user="postgres",
                            password="1234",
                            port=5432)

class BaseModel(Model):
    
    class Meta:
        database = db
        
        
class User(BaseModel):
    user_id = BigIntegerField(primary_key=True)
    nickname = TextField(null=True)
    
    
class Server(BaseModel):
    server_id = BigIntegerField(primary_key=True)
    server_name = TextField()
    server_owner = TextField(null=True)
    member_count = IntegerField()
    
    
class Msg(BaseModel):
    msg_id = BigAutoField(primary_key=True)
    user_id = ForeignKeyField(User, to_field='user_id')
    channel_id = BigIntegerField()
    server_id = ForeignKeyField(Server, to_field='server_id')
    date_msg = DateTimeField()
   
    
class BackupLog(BaseModel):
    backup_id = BigAutoField(primary_key=True)
    file_name = TextField(null=True)
    # file_size = IntegerField()
    backup_date = DateTimeField(null=True)
    status = TextField(null=True)
    
    
# class VoiceStat(BaseModel):
#     vc_id = BigAutoField()
#     user_id = ForeignKeyField(User, to_field='user_id')
#     datetime_joined = DateTimeField()
#     datetime_left = DateTimeField(null=True)
#     time_spent = BigIntegerField(null=True)
    

if __name__ == '__main__':
    creation_table = False
    create_backup_logs = True
    add_index = True

    # penser à créer la base de données sur postgreSQL
    if creation_table:
        # pour créer la table
        db.drop_tables([User, Server, Msg, BackupLog])
        db.create_tables([User, Server, Msg, BackupLog])
    
    if create_backup_logs:
        db.create_tables([BackupLog])
        
    if add_index:
        Msg.add_index(Msg.server_id)
